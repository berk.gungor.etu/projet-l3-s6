from Node import Node
from copy import copy
class Btree:
    def __init__(self,root=None,k= None):

        """
        Initialise l'arbre

        Args:
            root (Node, optional): Le nœud racine de l'arbre B. Par défaut à None.
            k (int, optional): Le degré minimum de l'arbre B. Par défaut à 3.

        """
        if k is None:
            k = 3
        self.k = k
        self.l = k//2 + 1
        self.u = k+1
        self.root = root

    def getRoot(self):
        """
        Retourne la racine de l'arbre B.

        Returns:
            Node: Le nœud racine de l'arbre B.

 
        """
        return self.root
    
    def size(self, target : Node):
        """
        Calcule la taille (nombre de clés) d'un nœud ou d'un sous-arbre.

        Args:
            target (Node): Le nœud ou sous-arbre dont la taille est calculée.

        Returns:
            int: Le nombre total de clés dans le nœud ou le sous-arbre.

 
        """
        if target.leaf:
            return len(target.keys)
        else:
            return sum(map(self.size, target.childs)) + len(target.keys)
        

    def min_depth(self, node: Node):
        """
        Calcule la profondeur minimale de l'arbre ou d'un sous-arbre à partir d'un nœud donné.

        Args:
            node (Node): Le nœud à partir duquel la profondeur minimale est calculée.

        Returns:
            int: La profondeur minimale de l'arbre ou du sous-arbre.
 
        """
        if node.leaf:
            return 0  
        return 1 + min(self.min_depth(child) for child in node.childs)

    def max_depth(self, node: Node):
        """
        Calcule la profondeur maximale de l'arbre ou d'un sous-arbre à partir d'un nœud donné.

        Args:
            node (Node): Le nœud à partir duquel la profondeur maximale est calculée.

        Returns:
            int: La profondeur maximale de l'arbre ou du sous-arbre.
        """
        if node.leaf:
            return 0
        return 1 + max(self.max_depth(child) for child in node.childs)

    def isBalanced(self,target : Node):
        """
        Vérifie si l'arbre ou le sous-arbre est équilibré, c'est-à-dire si la profondeur minimale et maximale sont égales.

        Args:
            target (Node): Le nœud racine de l'arbre ou du sous-arbre à vérifier.

        Returns:
            bool: True si l'arbre ou le sous-arbre est équilibré, False sinon.
        """
        if target is None:
            return True  
        return self.min_depth(target) == self.max_depth(target)

    def isSorted(self, node):
        """
        Vérifie si les clés d'un nœud sont triées.

        Args:
            node (Node): Le nœud à vérifier.

        Returns:
            bool: True si les clés du nœud sont triées, False sinon.
        """
        return all(node.keys[i] <= node.keys[i + 1] for i in range(len(node.keys) - 1))
    
    def isBtree(self,target=None):
        """
        Vérifie si la structure ciblée ou l'arbre entier respecte les propriétés d'un arbre B.

        Args:
            target (Node, optional): Le nœud cible à partir duquel la vérification commence. Par défaut, c'est la racine.

        Returns:
            bool: True si la structure est un arbre B valide, False sinon. 
        """
        if target is None:
            target = self.root  
        if not self.isSorted(target):
            return False
        if not self.isBalanced(target):
            return False
        if not target.leaf:
            for child in target.childs:
                if not self.isBtree(child):
                    return False
        
        return True
     
    def search(self,key):
        """
        Recherche une clé dans l'arbre B et retourne le nœud la contenant, si elle existe.

        Args:
            key: La clé à rechercher.

        Returns:
            Node or None: Le nœud contenant la clé, ou None si la clé n'est pas trouvée. 
        """
        return self.root.search(key)
    
    def linearisation(self):
        """
        Linearise l'arbre B en une liste triée de toutes ses clés.

        Returns:
            list: Une liste contenant toutes les clés de l'arbre triées. 
        """
        result = self.root.linearisation()
        return result
    
    def removeNode(self,target):
        """
        Supprime un nœud de la liste des enfants de son parent.

        Args:
            target (Node): Le nœud à supprimer. 
        """
        target.parent.childs.remove(target)

    def updateParents(self,l ,p : Node, atStart = False):
        """
        Change les parents d'une liste de nœuds.

        Args:
            l (list): La liste des nœuds dont les parents doivent être changés.
            p (Node): Le nouveau parent pour les nœuds dans la liste.
            atBeg (bool, optional): Si True, les nœuds sont ajoutés au début de la liste des enfants du parent. Sinon, ils sont ajoutés à la fin.
        """
        lcopy = copy(l)
        if atStart:
            for i in reversed(l):
                self.removeNode(i)
                i.parent = p
                p.childs.insert(0,i)      
        else:
            for i in (lcopy):
                self.removeNode(i)
                i.parent = p
                p.childs.append(i)

    def getParentIndex(self,target : Node):
        """
        Obtient l'indice d'un nœud cible parmi les enfants de son parent.

        Args:
            target (Node): Le nœud cible dont l'indice est recherché.

        Returns:
            int: L'indice du nœud cible dans la liste des enfants de son parent. 
        """
        return target.parent.childs.index(target)
    
    def parentLowerBound(self, target : Node):
        """
        Trouve la clé frontière inférieure dans le parent pour un nœud donné.

        Args:
            target (Node): Le nœud pour lequel la borne inférieure est recherchée.

        Returns:
            La clé de borne inférieure ou None si aucune borne n'est trouvée. 
        """
        index = self.getParentIndex(target)
        if index == 0:
            return None
        else:
            return target.parent.keys[index-1]
    
    def parentUpperBound(self,target : Node):
        """
        Trouve la clé frontière supérieure dans le parent pour un nœud donné.

        Args:
            target (Node): Le nœud pour lequel la borne supérieure est recherchée.

        Returns:
            La clé de borne supérieure ou None si aucune borne n'est trouvée. 
        """
        index = self.getParentIndex(target)
        if index == len(target.parent.childs)-1:
            return None
        else:
            return target.parent.keys[index]
    
    

    def getLeftNode(self,target : Node):
        """
        Obtient le nœud voisin gauche d'un nœud cible.

        Args:
            target (Node): Le nœud cible dont le voisin gauche est recherché.

        Returns:
            Node: Le nœud voisin à gauche. 
        """
        return target.parent.childs[self.getParentIndex(target)-1]


    def getRightNode(self,target : Node):
        """
        Obtient le nœud voisin droit d'un nœud cible.

        Args:
            target (Node): Le nœud cible dont le voisin droit est recherché.

        Returns:
            Node: Le nœud voisin à droite.
        """
        return target.parent.childs[self.getParentIndex(target)+1]

   
    def insertNodeSorted(self, target: Node, key):
        """
        Insère une clé dans un nœud en maintenant l'ordre des clés.

        Args:
            target (Node): Le nœud dans lequel insérer la clé.
            key: La clé à insérer.

        Returns:
            int: L'indice où la clé a été insérée. 
        """
        if target.size() == 0:
            target.keys = [key]
        
        for i, k in enumerate(target.keys):
            if key < k:
                target.keys.insert(i, key)
                break
        if key>k:
            target.keys.append(key)
            i+=1
        return i    

  
    def insertBtree(self,target : Node, key):
        """
        Insère une clé dans l'arbre B en commençant par le nœud cible.

        Args:
            target (Node): Le nœud où commencer l'insertion.
            key: La clé à insérer. 
        """
        if target.leaf:
            self.insertNodeSorted(target,key)
        else:
            for j,k in enumerate(target.keys):
                inserted = False
                if key <= k:
                    self.insertBtree(target.childs[j], key)
                    inserted = True
                    break
            if not inserted:
                self.insertBtree(target.childs[j+1], key)
        if target.size() == self.u:
            mid = target.size()//2
            if target.parent != None:
                index = self.insertNodeSorted(target.parent,target.keys[mid])
                removed_keys = target.keys[mid+1:]
                target.keys = target.keys[:mid]
                new_node = Node(target.parent,removed_keys)
                target.parent.childs.insert(index + 1, new_node)
                if not target.leaf:
                    new_node.leaf = False
                self.updateParents(target.childs[mid+1:],new_node)

            else:
                midl = target.keys[:mid]
                midr = target.keys[mid+1:]
                target.keys = [target.keys[mid]]
                childsl = target.childs[:mid+1]
                childsr = target.childs[mid+1:]
                nodel = Node(target,midl)
                noder = Node(target,midr)
                if len(childsl) > 0:
                    nodel.leaf = False
                if len(childsr) > 0:
                    noder.leaf = False
                self.updateParents(childsl,nodel)
                self.updateParents(childsr,noder)
                target.childs = [nodel,noder]
                target.leaf = False

    def insert(self, key):
        """
        Insère une clé dans l'arbre B en commençant par la racine.

        Args:
            key: La clé à insérer. 
        """
        self.insertBtree(self.root, key)

    def insertlist(self, keys):
        """
        Insère une liste de clés dans l'arbre B.

        Args:
            keys (list): Les clés à insérer. 
        """
        for i,k in enumerate(keys):
            self.insertBtree(self.root,k)

    def mergeNeighbours(self, target : Node, neigh : Node, inverseKeyOrder = False):
        """
        Fusionne un nœud avec son voisin.

        Args:
            target (Node): Le nœud cible à fusionner.
            neigh (Node): Le nœud voisin à fusionner avec le nœud cible.
            inverseKeyOrder (bool, optional): Si True, fusionne en inversant l'ordre des clés. 
        """
        if not inverseKeyOrder:
            target.keys = (target.keys + neigh.keys)
            self.updateParents(neigh.childs,target)
        else:
            target.keys = neigh.keys + target.keys
            self.updateParents(neigh.childs,target,True)
        self.removeNode(neigh)

    def predecesseur(self,target : Node):
        """
        Trouve le prédécesseur d'un nœud donné.

        Args:
            target (Node): Le nœud pour lequel trouver le prédécesseur.

        Returns: 
            Node: Le nœud predecesseur
        """
        if target.leaf:
            return target
        else:
            return self.predecesseur(target.childs[-1])
        
    def successeur(self,target : Node):
        """
        Trouve le successeur d'un nœud donné.

        Args:
            target (Node): Le nœud pour lequel trouver le successeur.

        Returns:
            Node: Le nœud successeur. 
        """
        if target.leaf:
            return target
        else:
            return self.successeur(target.childs[0])
    
    

    def decaleL(self,target : Node):
        """
        Décale les clés à gauche dans le nœud cible en redistribuant les clés entre le nœud et son voisin gauche.

        Args:
            target (Node): Le nœud cible pour le décalage.


        """
        indexparent = self.getParentIndex(target)
        parentKey = self.parentLowerBound(target)

        del target.parent.keys[indexparent-1]
        target.parent.keys.insert(indexparent-1,target.keys[0])
        del target.keys[0]
        self.getLeftNode(target).keys.insert(parentKey)

    def decaleR(self,target : Node):
        """
        Décale les clés à droite dans le nœud cible en redistribuant les clés entre le nœud et son voisin droit.

        Args:
            target (Node): Le nœud cible pour le décalage.
        """
        indexparent = self.getParentIndex(target)
        parentKey = self.parentUpperBound(target)
        
        del target.parent.keys[indexparent]
        target.parent.keys.insert(indexparent,target.keys[-1])
        del target.keys[-1]
        self.getRightNode(target).keys.insert(0,parentKey)

    def monteL(self,target : Node):
        """
        Monte le premier élément du nœud cible vers son parent à gauche, utilisé lors des rééquilibrages.

        Args:
            target (Node): Le nœud cible pour l'opération.

     
        """
        indexparent = self.getParentIndex(target)

        target.parent.keys.insert(indexparent-1,target.keys[0])
        del target.keys[0]

    def monteR(self,target : Node):
        """
        Monte le dernier élément du nœud cible vers son parent à droite, utilisé lors des rééquilibrages.

        Args:
            target (Node): Le nœud cible pour l'opération.
        """
        indexparent = self.getParentIndex(target)
        
        target.parent.keys.insert(indexparent,target.keys[-1])
        del target.keys[-1]


    def suppressionBtree(self, target : Node, key):
        """
        Supprime une clé dans l'arbre B

        Args:
            target (Node): Le nœud cible où la suppression commence.
            key: La clé à supprimer.
 
        """
        if target.leaf:

            index = target.keys.index(key)
            del target.keys[index]

            if target.parent is None and target.size() < self.l - 1:
                indexparent = self.getParentIndex(target)
                if indexparent != 0 and self.getLeftNode(target).size() > self.l - 1:
                    self.decaleR(self.getLeftNode(target))
                elif indexparent < len(target.parent.childs) - 1 and self.getRightNode(target).size() > self.l - 1:
                    self.decaleL(self.getRightNode(target))   
                else:
                    if indexparent!= 0:
                        target.keys.insert(0,target.parent.keys[indexparent-1])
                        del target.parent.keys[indexparent-1]
                        self.mergeNeighbours(target,self.getLeftNode(target),True)
                    else:
                        target.keys.append(target.parent.keys[indexparent])
                        del target.parent.keys[indexparent]
                        self.mergeNeighbours(target,self.getRightNode(target))

            return True
        
        else:
            supprime = False

            for i,k in enumerate(target.keys):
                if key < k:
                    deleteSuccess = self.suppressionBtree(target.childs[i], key)
                    supprime = True
                    break

                elif key == k:

                    filsg = target.childs[i]
                    filsd = target.childs[i+1]
                    del target.keys[i]
                    predecesseur : Node= self.predecesseur(filsg)
                    successeur = self.successeur(filsd)

                    if predecesseur is filsg and successeur is filsd:

                        if filsg.size() > self.l - 1:
                            self.monteR(filsg)
                        elif filsd.size() > self.l - 1:
                            self.monteL(filsd)   
                        else:

                            self.mergeNeighbours(filsg,filsd)
                    else:
                        self.insertNodeSorted(target,predecesseur.keys[-1])
                        self.suppressionBtree(filsg,predecesseur.keys[-1])
                    deleteSuccess = True
                    supprime = True
                    break

            if not supprime:
                deleteSuccess = self.suppressionBtree(target.childs[-1], key)

            if target.size() < self.l - 1:

                if target.parent is None:
                    if len(target.childs) == 1:
                        self.root = target.childs[0]
                        self.root.parent = None
                    return True
                
                indexparent = self.getParentIndex(target)

                if indexparent != 0 and self.getLeftNode(target).size() > self.l - 1:
                    self.decaleR(self.getLeftNode(target))
                    self.updateParents([self.getLeftNode(target).childs[-1]],target,True)

                elif indexparent < len(target.parent.childs) - 1 and self.getRightNode(target).size() > self.l - 1:
                    self.decaleL(self.getRightNode(target))
                    self.updateParents([self.getRightNode(target).childs[0]],target)

                else:
                    if indexparent != 0:
                        target.keys.insert(0,target.parent.keys[indexparent-1])
                        del target.parent.keys[indexparent-1]
                        nouvelNode = self.getLeftNode(target)
                        self.mergeNeighbours(target,nouvelNode,True)

                    else:
                        target.keys.append(target.parent.keys[indexparent])
                        del target.parent.keys[indexparent]
                        nouvelNode = self.getRightNode(target)
                        
                        self.mergeNeighbours(target,nouvelNode)
                        
            return deleteSuccess   
                
    def suppression(self,key):
        """
        Supprime une clé de l'arbre en commençant par la racine.

        Args:
            key: La clé à supprimer.
 
        """
        return self.suppressionBtree(self.root,key)  