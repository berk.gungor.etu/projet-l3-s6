from Btree import *
from Node import *
import unittest


class Test_btree(unittest.TestCase):
    def setUp(self):
        self.n1 = Node(None, [])
        self.n2 = Node(None,[])
        self.n3 = Node(None,[])
        self.btree = Btree(self.n1, 10)

    def test_still_btree_after_insertion2(self ):
        self.btree.insertlist(range(1,100000))
        self.assertTrue(self.btree.isBtree())

    def test_suppression_ordre(self):
        self.btree.insertlist(range(1,100000))
        for i in range(1,500):
            self.btree.suppression(10*i)
            self.assertTrue(self.btree.isBtree())

    def test_suppression_ordre2(self):
        self.btree.insertlist(range(1,100000))       
        for i in range(1,500):
            self.btree.suppression((10*i)-5)
            self.assertTrue(self.btree.isBtree())

if __name__ == '__main__':
    unittest.main()