from Btree import *
from Node import *
import unittest


class Test_btree(unittest.TestCase):
    def setUp(self):
        self.n1 = Node(None, [])
        self.n2 = Node(None,[])
        self.n3 = Node(None,[])
        self.btree = Btree(self.n1, 2)

    def testSearchExisting(self):
        self.btree.insertlist(range(1,14))
        self.assertTrue(self.btree.search(2))

    def testSearchNonExisting(self):
        self.btree.insertlist(range(1,14))
        self.assertFalse(self.btree.search(22))

    def testSearchEmptyTree(self):
        self.assertFalse(self.btree.search(2))

    def testSplittedAfterInsertion(self):
        self.assertEqual(0,len(self.n1.childs))
        self.btree.insertlist([1,2])
        self.assertEqual(0,len(self.n1.childs))
        self.btree.insertBtree(self.n1,3)         
        self.assertEqual(2,len(self.n1.childs))   

    def test_still_btree_after_insertion(self ):
        self.btree.insertlist(range(1,14))
        self.assertTrue(self.btree.isBtree())
        self.btree.insertlist([20,222])
        self.assertTrue(self.btree.isBtree())

    def test_btree_is_balanced(self):
        self.btree.insertlist(range(100,0,-1))
        root = self.btree.getRoot()
        self.assertEqual(self.btree.max_depth(root), self.btree.min_depth(root))
        self.assertTrue(self.btree.isBalanced(root))


    def test_exp(self):
        self.btree.insert(2)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(4)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(5)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(6)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(8)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(10)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(12)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(14)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(16)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(18)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(20)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(22)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(24)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(26)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(28)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(30)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(32)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(34)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(36)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(7)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(9)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(11)
        self.assertTrue(self.btree.isBtree())
        self.btree.insert(13)
        self.assertTrue(self.btree.isBtree())
        self.btree.suppression(14)
        self.assertTrue(self.btree.isBtree())
        self.btree.suppression(10)
        self.assertTrue(self.btree.isBtree())
        self.btree.suppression(20)
        self.assertTrue(self.btree.isBtree())
        self.btree.suppression(18)
        self.assertTrue(self.btree.isBtree())
        self.btree.suppression(16)
        self.assertTrue(self.btree.isBtree())
        self.btree.suppression(24)
        self.assertTrue(self.btree.isBtree())
        self.btree.suppression(6)
        self.assertTrue(self.btree.isBtree())

if __name__ == '__main__':
    unittest.main()