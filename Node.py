class Node:
    def __init__(self, parent = None,keys = []):
        """
        Initialise un nœud de l'arbre B.

        Args:
            parent (Node, optional): Le parent de ce nœud. Par défaut à None.
            keys (list, optional): Les clés stockées dans le nœud. Par défaut à une liste vide. 
        """
        self.leaf = True
        self.keys = keys or []
        self.parent = parent
        self.childs = []

    def size(self):
        """
        Retourne le nombre de clés dans le nœud.

        Returns:
            int: Le nombre de clés. 
        """
        return len(self.keys)
    
    def isEmpty(self):
        """
        Vérifie si le nœud est vide (sans clés).

        Returns:
            bool: True si le nœud est vide, False sinon. 
        """
        return self.size() == 0

    def search(self,key):
        """
        Recherche une clé dans le nœud ou récursivement dans ses enfants.

        Args:
            key: La clé à rechercher.

        Returns:
            bool: True si la clé est trouvée, False sinon. 
        """
        if self.leaf:
            return (key in self.keys)
        else:
            for i,k in enumerate(self.keys):
                if key < k:
                    return self.childs[i].search(key)
                elif key == k:
                    return True
            if key > k:
                return self.childs[-1].search(key)
        return False
    
    def linearisation(self):
        """
        Crée une liste linéaire des clés à partir du sous-arbre dont ce nœud est la racine.

        Returns:
            list: Une liste triée de toutes les clés dans le sous-arbre. 
        """
        result = []
        if not self.leaf:
            for i in range(len(self.keys)):
                result += self.childs[i].linearisation()
                result.append(self.keys[i])
            result += self.childs[-1].linearisation()
        else:
            result += self.keys
        return result
    
    