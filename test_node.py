from Btree import Btree
from Node import Node

import unittest

class Test_node(unittest.TestCase):
    
    def setUp(self):
        self.n1 = Node(None,[4])
        self.n2_1 = Node(self.n1,[2])
        self.n2_2 = Node(self.n1,[6])
        self.n3_1_1 = Node(self.n2_1,[1])
        self.n3_1_2 = Node(self.n2_1,[3])
        self.n3_2_1 = Node(self.n2_2,[5])
        self.n3_2_2 = Node(self.n2_2,[7])
        
        self.n1.childs.extend([self.n2_1, self.n2_2])
        self.n2_1.childs.extend([self.n3_1_1, self.n3_1_2])    
        self.n2_2.childs.extend([self.n3_2_1, self.n3_2_2])    
        
        self.n1.leaf = False
        self.n2_1.leaf = False
        self.n2_2.leaf = False
        
        self.n2_1.parent   = self.n1
        self.n2_2.parent   = self.n1
        self.n3_1_1.parent = self.n2_1
        self.n3_1_2.parent = self.n2_1
        self.n3_2_1.parent = self.n2_2
        self.n3_2_2.parent = self.n2_2
        
        self.btree = Btree(self.n1) 
        self.empty_node = Node(None,[])
        self.root = self.n1


    def test_search_non_existing_element(self):
        self.assertFalse(self.btree.search(22))

    def test_search_non_existing_element2(self):
        keys_to_insert = [4, 2, 6, 1, 3, 5, 7]
        for key in keys_to_insert:
            self.btree.insert(key)
        self.assertFalse(self.btree.search(22))

    def test_search_existing_element(self):
        self.assertTrue(self.btree.search(5))


    def test_nodes_not_empty(self):
        self.assertFalse(self.n1.isEmpty())
        self.assertFalse(self.n2_1.isEmpty())

    def test_search_existing_element2(self):
        keys_to_insert = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22]
        for key in keys_to_insert:
            self.btree.insert(key)
        self.assertTrue(self.btree.search(20))

    def test_insertion(self):
        keys_to_insert = [4, 2, 6, 1, 3, 5, 7]
        for key in keys_to_insert:
            self.btree.insert(key)
        self.assertEqual(self.root.keys, [4]) 
        self.assertEqual(len(self.root.childs), 2)
        self.assertEqual(self.root.childs[0].keys, [2]) 
        self.assertEqual(self.root.childs[1].keys, [6])

    def test_search_tree(self):
        keys_to_insert = []
        for key in keys_to_insert:
            self.btree.insert(key)
        self.assertTrue(self.btree.search(3))
    
if __name__ == '__main__':
    unittest.main()