# Depot de Projet S6 de GUNGOR Berk

# Arbre B

## Comment tester?
    Apres avoir cloné le git il faut se placer dans projet-l3-s6 et lancer ces commandes:


    $ python3 -m unittest test_btree.py

    $ python3 -m unittest test_btree2.py

    $ python3 -m unittest test_node.py

## UML de ce projet

![UML](projet.png)
